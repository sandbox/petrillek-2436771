/**
 * We are passing DRUPAL.settings to JS.
 *
 * @type {{attach: attach}}
 */

Drupal.behaviors.jQueryScrollit = {
  attach: function (context, settings) {
    console.log('TEST START');
    console.log(settings.jquery_scrollit.easing);
    console.log(settings.jquery_scrollit.scrollTime);
    console.log(settings.jquery_scrollit.topOffset);
    console.log('TEST STOP');
    (function ($) {
      var targetnum = $(settings.jquery_scrollit.targetNum);
      var navnum = $(settings.jquery_scrollit.navNum);

      targetnum.each(function (i) {
        $(this).attr('data-scroll-index', i);
        ++i;
      });
      navnum.each(function (x) {
        $(this).attr('data-scroll-nav', x);
        ++x;
      });

      $.scrollIt({
        upKey: settings.jquery_scrollit.upKey,
        downKey: settings.jquery_scrollit.downKey,
        easing: settings.jquery_scrollit.easing,
        scrollTime: settings.jquery_scrollit.scrollTime,
        activeClass: settings.jquery_scrollit.activeClass,
        onPageChange: settings.jquery_scrollit.onPageChange,
        topOffset: settings.jquery_scrollit.topOffset
      });
    })(jQuery);
  }
};
