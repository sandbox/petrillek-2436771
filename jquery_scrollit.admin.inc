<?php
/**
 * Page callback: jQuery ScrollIt settings.
 *
 * @see jquery_scrollit_menu()
 */

/**
 * Implements hook_form().
 */
function jquery_scrollit_form($form, &$form_state) {
  $form['selector_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Selector settings'),
    '#weight' => 1,
    '#collapsible' => FALSE,
  );
  $form['selector_settings']['jquery_scrollit_targetnum'] = array(
    '#type' => 'textfield',
    '#title' => t('Target class'),
    '#default_value' => variable_get('jquery_scrollit_targetnum', ''),
    '#description' => t('Provide a jQuery selector for direct target item. Example: #main .paragpraph-item'),
    '#required' => FALSE,
  );
  $form['selector_settings']['jquery_scrollit_navnum'] = array(
    '#type' => 'textfield',
    '#title' => t('Navigation source'),
    '#default_value' => variable_get('jquery_scrollit_navnum', ''),
    '#description' => t('Provide a jQuery selector for direct target item. Example:#main-menu a'),
    '#required' => FALSE,
  );

  /* Fieldset for ScrollIt Settings */
  $form['scrollit_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('ScrollIt settings'),
    '#weight' => 2,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['scrollit_settings']['jquery_scrollit_upkey'] = array(
    '#type' => 'textfield',
    '#title' => t('Up Key'),
    '#default_value' => variable_get('jquery_scrollit_upkey', '38'),
    '#description' => t('The code for Up key when navigating with keyboard. For list of available keys visit <a href="@url">http://www.javascriptkeycode.com</a>', array('@url' => url('http://www.javascriptkeycode.com'))),
    '#required' => FALSE,
  );
  $form['scrollit_settings']['jquery_scrollit_downkey'] = array(
    '#type' => 'textfield',
    '#title' => t('Down key'),
    '#default_value' => variable_get('jquery_scrollit_downkey', '40'),
    '#description' => t('The code for Down key when navigating with keyboard. For list of available keys visit <a href="@url">http://www.javascriptkeycode.com</a>', array('@url' => url('http://www.javascriptkeycode.com'))),
    '#required' => FALSE,
  );
  $form['scrollit_settings']['jquery_scrollit_easing'] = array(
    '#type' => 'textfield',
    '#title' => t('Easing'),
    '#default_value' => variable_get('jquery_scrollit_easing', 'linear'),
    '#description' => t('The value for easing. Default is linear.'),
    '#required' => FALSE,
  );
  $form['scrollit_settings']['jquery_scrollit_scrolltime'] = array(
    '#type' => 'textfield',
    '#title' => t('Scroll Time'),
    '#default_value' => variable_get('jquery_scrollit_scrolltime', '600'),
    '#description' => t('Set the speed of scrolling. Default is 600.'),
    '#required' => FALSE,
  );
  $form['scrollit_settings']['jquery_scrollit_activeclass'] = array(
    '#type' => 'textfield',
    '#title' => t('Active Class'),
    '#default_value' => variable_get('jquery_scrollit_activeclass', 'active'),
    '#description' => t('This class will be used on active menu items. Default is active.'),
    '#required' => FALSE,
  );
  $form['scrollit_settings']['jquery_scrollit_topoffset'] = array(
    '#type' => 'textfield',
    '#title' => t('Top Offset'),
    '#default_value' => variable_get('jquery_scrollit_topoffset', '0'),
    '#description' => t('How many pixels should be the top menu pushed down. Default is 0.'),
    '#required' => FALSE,
  );

  return system_settings_form($form);
}

/**
 * Implements hook_validate().
 */
function jquery_scrollit_form_validate($form, &$form_state) {
  if (($form_state['values']['jquery_scrollit_scrolltime'] == '')) {
    $form_state['values']['jquery_scrollit_scrolltime'] = 600;
  }
  if (($form_state['values']['jquery_scrollit_upkey'] == '')) {
    $form_state['values']['jquery_scrollit_upkey'] = 38;
  }
  if (($form_state['values']['jquery_scrollit_downkey'] == '')) {
    $form_state['values']['jquery_scrollit_downkey'] = 40;
  }
  if (($form_state['values']['jquery_scrollit_easing'] == '')) {
    $form_state['values']['jquery_scrollit_easing'] = 'linear';
  }
  if (($form_state['values']['jquery_scrollit_activeclass'] == '')) {
    $form_state['values']['jquery_scrollit_activeclass'] = 'active';
  }
  if (($form_state['values']['jquery_scrollit_topoffset'] == '')) {
    $form_state['values']['jquery_scrollit_topoffset'] = 0;
  }
  if (!is_numeric($form_state['values']['jquery_scrollit_scrolltime'])) {
    form_set_error('jquery_scrollit_scrolltime', t('Scroll Time must be Numeric'));
  }
  if (!is_numeric($form_state['values']['jquery_scrollit_topoffset'])) {
    form_set_error('jquery_scrollit_topoffset', t('Top Offset must be Numeric'));
  }
}
