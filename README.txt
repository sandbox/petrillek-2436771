General information
==========================
This module provides integration of ScrollIt library
(http://www.bytemuse.com/scrollIt.js) which allows for smooth scrolling
from source items to target items on the same page.

Instalation & usage
==========================
1. Install and enable the module as normal (you can visit
https://www.drupal.org/documentation/install/modules-themes for more
information)
2. Download the ScrollIt library from https://github.com/cmpolis/scrollIt.js
3. Extract the library to 'sites/all/libraries/scrollit' –> the correct
path to the js file is 'sites/all/libraries/scrollit/scrollIt.min.js'
(notice capital letter 'I').
4. Important step! Go to 'admin/config/user-interface/jquery-scrollit'
and fill in classes for source and target items.
E.g. Menu items as source and HTML5 sections as target.
5. On settings page you can also change the JS values like speed, keyboard
navigation codes etc.

Requirements
==========================
Libraries
jQuery Update

CONTACT
==========================
Petr Illek (petr-illek)
http://drupal.org/user/420365
petr@morpht.com
